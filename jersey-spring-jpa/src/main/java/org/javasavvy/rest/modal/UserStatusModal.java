package org.javasavvy.rest.modal;

public class UserStatusModal {
	
	private String status;
	private String message;
	private UserModal user;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public UserModal getUser() {
		return user;
	}
	public void setUser(UserModal user) {
		this.user = user;
	}
	
	

}
