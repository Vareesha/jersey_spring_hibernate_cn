package org.javasavvy.rest.services;


import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.javasavvy.rest.dao.UserDAO;
import org.javasavvy.rest.entity.BusinessUnit;
import org.javasavvy.rest.entity.KPI;
import org.javasavvy.rest.entity.User;
import org.javasavvy.rest.modal.UserModal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("userService")
@Transactional(propagation=Propagation.REQUIRED)
public class UserServiceImpl implements UserService {

	
	@Autowired(required=true)
	private UserDAO userDAO;
	
	
	public List<User> getUser(String name) {
		return userDAO.getUser(name);
	}
	
	public String getStatusProcedure(String name,String pwd){
		return userDAO.getStatusProcedure(name,pwd);
	}
	public List<UserModal> getUserProcedure() throws SQLException{
		return userDAO.getUserProcedure();
	}
	public List<KPI> getKPIValues(String view,String bu,String month,String year){
		return userDAO.getKPIValues(view, bu, month, year);
	}
	public List<BusinessUnit> getBusinessUnit(){
		return userDAO.getBusinessUnit();
	};




}
