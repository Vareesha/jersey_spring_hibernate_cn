package org.javasavvy.rest.services;

import java.sql.SQLException;
import java.util.List;

import org.javasavvy.rest.entity.BusinessUnit;
import org.javasavvy.rest.entity.KPI;
import org.javasavvy.rest.entity.User;
import org.javasavvy.rest.modal.UserModal;
import org.springframework.stereotype.Service;


public interface UserService {
	
	
	public List<User> getUser(String name);
	public String getStatusProcedure(String name,String pwd);
	public List<UserModal> getUserProcedure() throws SQLException;
	public List<KPI> getKPIValues(String view,String bu,String month,String year);
	public List<BusinessUnit> getBusinessUnit();
	
}
