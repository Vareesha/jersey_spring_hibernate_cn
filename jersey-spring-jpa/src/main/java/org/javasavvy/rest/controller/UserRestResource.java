package org.javasavvy.rest.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;

import org.glassfish.jersey.internal.util.Base64;
import org.javasavvy.rest.entity.BusinessUnit;
import org.javasavvy.rest.entity.KPI;
import org.javasavvy.rest.entity.User;
import org.javasavvy.rest.modal.MailParms;
import org.javasavvy.rest.modal.UserModal;
import org.javasavvy.rest.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ch.qos.logback.core.status.Status;

@Component
@Path("/user")
public class UserRestResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserRestResource.class);

	@Value("${name}")
	public String usernameProp;

	public String getUsername() {
		return usernameProp;
	}

	public void setUsername(String usernameProp) {
		this.usernameProp = usernameProp;
	}

	@Autowired(required = true)
	@Qualifier("userService")
	private UserService userService;

	Gson gson = new GsonBuilder().setPrettyPrinting().create();

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/user-json-meta")
	public UserModal getUser() {

		UserModal userModal = new UserModal();
		userModal.setName("jajfaddf");
		userModal.setPwd("jayaram");
		userModal.setStatus("poks");
		return userModal;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/userstatus/{name}/{pwd}")
	public Response getUserByNamePwd(@PathParam("name") String name, @PathParam("pwd") String pwd) {
		String status = userService.getStatusProcedure(name, pwd);
		UserModal userStatus = new UserModal();
		userStatus.setName(name);
		userStatus.setStatus(status);

		return Response.status(201).entity(gson.toJson(userStatus)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getUserByName")
	public List<User> getUserByName(UserModal userModal) {
		List<User> userList = userService.getUser(userModal.getName());
		LOGGER.debug("from property file=======================" + usernameProp);
		return userList;

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/userDetails")
	public Response getUserFromProc() throws SQLException {
		List<UserModal> userList = userService.getUserProcedure();
		return Response.status(201).entity(gson.toJson(userList)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, Authorization")
				.header("Access-Control-Allow-Credentials", "true").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/KPIValues")
	public Response getKPIValues(@QueryParam("view") String view, @QueryParam("bu") String bu,
			@QueryParam("month") String month, @QueryParam("year") String year) {
		List<KPI> kpiList = userService.getKPIValues(view, bu, month, year);
		return Response.status(201).entity(gson.toJson(kpiList)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/businessUnit")
	public Response getBusinessUnit(){
		List<BusinessUnit> bUnit = userService.getBusinessUnit();
		Set<String> buUniqueSet =new HashSet<>();
		for(BusinessUnit bu:bUnit){
			buUniqueSet.add(bu.getBusiness_unit());
		}
		return Response.status(201).entity(gson.toJson(buUniqueSet)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();
		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sendMail")
	public Response sendEmail(MailParms mailParms) throws IOException {

		LOGGER.debug("Sending mail...");
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		final String username = "vareeshaphone@gmail.com";
		final String password = "";
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		// Transport transport = session.getTransport();
		try {
			MimeMessage message = new MimeMessage(session);
//			message.setSubject("HTML  mail with images");
//			message.setFrom(new InternetAddress("vareeshaphone@gmail.com"));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress("vareeshaphone@gmail.com"));
			
			message.setSubject(mailParms.getSubject());
			message.setFrom(new InternetAddress(mailParms.getFrom()));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(mailParms.getTo()));
			

			// create a buffered image
			BufferedImage image = null;
			byte[] imageByte;

			// BASE64Decoder decoder = new BASE64Decoder();
			imageByte = Base64.decode(mailParms.getImage().getBytes());
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();

			// write the image to a file
			File outputfile = new File("image.png");
			ImageIO.write(image, "png", outputfile);
			
			
			
			// This HTML mail have to 2 part, the BODY and the embedded image
			
			MimeMultipart multipart = new MimeMultipart("related");

			// first part (the html)
			BodyPart messageBodyPart = new MimeBodyPart();
			String htmlText = "<H1>Hello</H1><img src=\"cid:image\">";
			messageBodyPart.setContent(htmlText, "text/html");

			// add it
			multipart.addBodyPart(messageBodyPart);

			// second part (the image)
			messageBodyPart = new MimeBodyPart();
		//	DataSource fds = new FileDataSource("C:/Users/vareesha.subbanna/Downloads/mail.png");
			DataSource fds = new FileDataSource(outputfile);
			messageBodyPart.setDataHandler(new DataHandler(fds));
			messageBodyPart.setHeader("Content-ID", "<image>");

			// add it
			multipart.addBodyPart(messageBodyPart);

			// put everything together
			message.setContent(multipart);

			// transport.connect();
			// transport.sendMessage(message,
			// message.getRecipients(Message.RecipientType.TO));
			Transport.send(message);
			// transport.close();
			return Response.status(201).entity(gson.toJson("Mail Successfully Delivered"))
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.header("Access-Control-Allow-Headers",
							"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
					.header("Access-Control-Allow-Credentials", "true").build();
		} catch (MessagingException ex) {
			LOGGER.debug("Stack Trace", ex);
			return Response.status(Status.ERROR).entity(gson.toJson("Mail cannot be Delivered"))
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.header("Access-Control-Allow-Headers",
							"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
					.header("Access-Control-Allow-Credentials", "true").build();

		}
	}
}
