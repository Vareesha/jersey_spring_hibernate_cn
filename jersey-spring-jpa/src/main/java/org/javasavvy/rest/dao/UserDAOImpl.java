package org.javasavvy.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.internal.SessionImpl;
import org.javasavvy.rest.entity.BusinessUnit;
import org.javasavvy.rest.entity.KPI;
import org.javasavvy.rest.entity.User;
import org.javasavvy.rest.modal.UserModal;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service("userDAO")
@Transactional(propagation=Propagation.REQUIRED)
public class UserDAOImpl implements UserDAO {
	
	
	@PersistenceContext
	public EntityManager entityManager;

	
	@Transactional(readOnly=true)
	public List<User> getUser(String name) {
		 Query query  = entityManager.createNamedQuery("fetchByName",User.class);
		 query.setParameter("name", name);
		 List<User> userList=query.getResultList();
		 return userList;
	}
	@Transactional(readOnly=true)
	public String getStatusProcedure(String name,String pwd){
		StoredProcedureQuery query = entityManager.createNamedStoredProcedureQuery("loginstatusproc");
		query.setParameter("USERNAME", name);
		query.setParameter("PASS", pwd);
		/*query.execute();*/
	 String outputParameterValue = (String)query.getOutputParameterValue("OUT_COUNT");
		return outputParameterValue;
		
	}
	@Transactional(readOnly=true)
	public List<UserModal> getUserProcedure() throws SQLException{

Connection connection = entityManager.unwrap(SessionImpl.class).connection();
					        CallableStatement prepareCall = connection.prepareCall("{CALL \"_SYS_BIC\".\"RAJProc/LOGIN_CALL\"(?)}");
					        prepareCall.execute();
					        	ResultSet rs = prepareCall.getResultSet(); 
			 List<UserModal> userList=new ArrayList<UserModal>();
					        	if (rs != null) {
					    			while(rs.next()){
					    				UserModal user =new UserModal();
					    				user.setPwd(rs.getString("PWD"));
					    				user.setName(rs.getString("USER_NAME"));
					    				user.setStatus(rs.getString("STATUS"));
					    				userList.add(user);
					    			}
					    		}
		return userList;
		
	}
	@SuppressWarnings("unchecked")
	public List<KPI> getKPIValues(String view,String bu,String month,String year){
		List<KPI> kpiList=entityManager.createNativeQuery("SELECT \"KPI_KEY\", \"CC_AP\", \"CC_AP_pcent\", sum(\"CC_ACTUAL\") AS \"CC_ACTUAL\", sum(\"CC_PLANNED\") AS \"CC_PLANNED\", sum(\"CC_FORECAST\") AS \"CC_FORECAST\" FROM \"_SYS_BIC\".\"RAJProc/Overview\"('PLACEHOLDER' = ('$$IP_VIEW$$','"+ view+"'), 'PLACEHOLDER' = ('$$IP_BU$$','"+ bu+"'), 'PLACEHOLDER' = ('$$IP_POPER$$', '"+month+"'), 'PLACEHOLDER' = ('$$IP_GJAHR$$','"+year +"')) GROUP BY \"KPI_KEY\", \"CC_AP\", \"CC_AP_pcent\"",KPI.class).getResultList();
		return kpiList;
		
	}
	
	public List<BusinessUnit> getBusinessUnit(){

		List<BusinessUnit> bUnit = (List<BusinessUnit>)entityManager.createNativeQuery("SELECT  * FROM \"HCP_RAJ\".\"BU_PR_MAP\" ",BusinessUnit.class).getResultList();
		return bUnit;
	}

}
