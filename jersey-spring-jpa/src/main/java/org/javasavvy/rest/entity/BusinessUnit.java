package org.javasavvy.rest.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "BU_PR_MAP",schema="HCP_RAJ")
@NamedQueries({
		@NamedQuery(name = "fetchDistinctCodeUnit", query = "select distinct e.bu_cd,e.business_unit from BusinessUnit e") })
public class BusinessUnit implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="PRCTR")
	private String PRCTR;
	private String bu_cd;
	private String business_unit;

	public String getBu_cd() {
		return bu_cd;
	}

	public void setBu_cd(String bu_cd) {
		this.bu_cd = bu_cd;
	}

	public String getBusiness_unit() {
		return business_unit;
	}

	public void setBusiness_unit(String business_unit) {
		this.business_unit = business_unit;
	}

	public String getPRCTR() {
		return PRCTR;
	}

	public void setPRCTR(String pRCTR) {
		PRCTR = pRCTR;
	}


}
