package org.javasavvy.rest.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
	



@Entity
@Table(name = "CORP_LOGIN",schema="HCP_RAJ" )
@NamedQueries({
		@NamedQuery(name = "fetchByName", query = "select e from User e where e.user_name=:name"),
		@NamedQuery(name = "fetchAll", query = "select u from User u") })
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name = "loginstatusproc", procedureName = "\"HCP_RAJ\".\"RAJProc::Corp_Login\"", 
			 parameters = {
					@StoredProcedureParameter(name = "USERNAME", type = String.class, mode = ParameterMode.IN),
					@StoredProcedureParameter(name = "PASS", mode = ParameterMode.IN, type = String.class),
					@StoredProcedureParameter(name = "OUT_COUNT", type = String.class, mode = ParameterMode.OUT) }) ,
	@NamedStoredProcedureQuery(name = "getUserProc", procedureName = "\"_SYS_BIC\".\"RAJProc/LOGIN_CALL\"", 
	resultClasses={User.class},
	 parameters = {
			@StoredProcedureParameter(name = "Out_put_Table", type = User.class, mode = ParameterMode.OUT) })
})
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="USER_NAME")
	private String user_name;
    @Column(name="PWD")
	private String pwd;
    @Column(name="STATUS")
	private String status;

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

}
