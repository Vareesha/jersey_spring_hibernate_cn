package org.javasavvy.rest.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RAJProc/Overview")
public class KPI implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "KPI_KEY")
	private String kpiKey;
	@Column(name = "CC_AP")
	private double cc_ap;
	@Column(name = "CC_AP_pcent")
	private double cc_ap_pcent;
	@Column(name = "CC_ACTUAL")
	private double cc_actual;
	@Column(name = "CC_PLANNED")
	private double cc_planned;
	@Column(name = "CC_FORECAST")
	private double cc_forecast;

	public String getKpiKey() {
		return kpiKey;
	}

	public void setKpiKey(String kpiKey) {
		this.kpiKey = kpiKey;
	}

	public double getCc_ap() {
		return cc_ap;
	}

	public void setCc_ap(double cc_ap) {
		this.cc_ap = cc_ap;
	}

	public double getCc_ap_pcent() {
		return cc_ap_pcent;
	}

	public void setCc_ap_pcent(double cc_ap_pcent) {
		this.cc_ap_pcent = cc_ap_pcent;
	}

	public double getCc_actual() {
		return cc_actual;
	}

	public void setCc_actual(double cc_actual) {
		this.cc_actual = cc_actual;
	}

	public double getCc_forecast() {
		return cc_forecast;
	}

	public void setCc_forecast(double cc_forecast) {
		this.cc_forecast = cc_forecast;
	}

	public double getCc_planned() {
		return cc_planned;
	}

	public void setCc_planned(double cc_planned) {
		this.cc_planned = cc_planned;
	}

	@Override
	public String toString() {
		return "KPI [kpiKey=" + kpiKey + ", cc_ap=" + cc_ap + ", cc_ap_pcent=" + cc_ap_pcent + ", cc_actual="
				+ cc_actual + ", cc_planned=" + cc_planned + ", cc_forecast=" + cc_forecast + "]";
	}
	

}
