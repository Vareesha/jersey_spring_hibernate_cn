package org.javasavvy.rest.autharization;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.internal.util.Base64;
public class SecurityFilter implements ContainerRequestFilter {
	
	private static final String AUTHORIZATION_HEADER_KEY="Authorization";
	private static final String AUTHORIZATION_HEADER_PREFIX="Basic";

	@Override
	public void filter(ContainerRequestContext requestContext ) throws IOException {
		List<String> authHeader = requestContext.getHeaders().get(AUTHORIZATION_HEADER_KEY);
		requestContext.getHeaders().add("Access-Control-Allow-Origin", "*");
		requestContext.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT,OPTIONS");
		requestContext.getHeaders().add("Access-Control-Allow-Credentials", "true");
		requestContext.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, Authorization");
		
		String username = null;
		String password=null;
		if( authHeader !=null &&authHeader.size()>0){
			String authString =authHeader.get(0);
			authString=authString.replace(AUTHORIZATION_HEADER_PREFIX, "").trim();
			String decodedString=Base64.decodeAsString(authString);
			String[] credential = decodedString.split(":");
			if(credential.length>1){
			 username = credential[0];
			 password = credential[1];
			}
			if ("User".equals(username) && "Password".equals(password)) {
				return ;
			}
		}
		Response unauthorizedStatus=Response.status(Response.Status.UNAUTHORIZED)
				.entity("User cannot access the resource")
				.build();
		requestContext.abortWith(unauthorizedStatus);
		
	}


}
