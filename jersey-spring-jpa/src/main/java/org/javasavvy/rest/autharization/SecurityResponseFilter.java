package org.javasavvy.rest.autharization;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
public class SecurityResponseFilter implements ContainerResponseFilter{

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
		responseContext.getStringHeaders().add("Access-Control-Allow-Origin", "*");
		responseContext.getStringHeaders().add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT,OPTIONS");
		responseContext.getStringHeaders().add("Access-Control-Allow-Credentials", "true");
		responseContext.getStringHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, Authorization");
	}

}
