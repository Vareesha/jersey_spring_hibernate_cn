package org.javasavvy.rest.controller.test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.javasavvy.rest.controller.UserRestResource;
import org.javasavvy.rest.entity.KPI;
import org.javasavvy.rest.entity.User;
import org.javasavvy.rest.modal.UserModal;
import org.javasavvy.rest.services.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
// @ContextConfiguration(locations =
// {"/applicationContext-jdbc.xml","/applicationContext.xml"})
public class UserRestResourceTest {

	@InjectMocks
	private UserRestResource controller;

	@Mock
	private UserService userService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getUserTest() {
		UserModal user = controller.getUser();
		Assert.assertEquals("jajfaddf", user.getName());
	}

	@Test
	public void getUserByNamePwd() {

		Mockito.when(userService.getStatusProcedure(Mockito.anyString(), Mockito.anyString())).thenReturn("Valid");
		Response userByNamePwd = controller.getUserByNamePwd("ADMIN", "ADMIN");
		Assert.assertNotNull(userByNamePwd);
		Assert.assertEquals(201, userByNamePwd.getStatus());
		Assert.assertNotNull("ACTIVE", userByNamePwd.getEntity());
	}

	@Test
	public void getUserByName() {
		UserModal userModel = new UserModal();
		userModel.setName("ADMIN");
		List<User> userDataList = new ArrayList<>();
		User user = new User();
		user.setUser_name("ADMIN");
		user.setStatus("ACTIVE");
		user.setPwd("ADMIN");
		userDataList.add(user);

		Mockito.when(userService.getUser(Mockito.anyString())).thenReturn(userDataList);
		List<User> userList = controller.getUserByName(userModel);
		Assert.assertNotNull(userList);
		Assert.assertEquals(1, userList.size());
		Assert.assertEquals("ACTIVE", userList.get(0).getStatus());
	}

	@Test
	public void getUserFromProc() throws SQLException {
		UserModal userModel = new UserModal();
		userModel.setName("ADMIN");

		List<UserModal> userDataList = new ArrayList<UserModal>();
		userDataList.add(userModel);
		Mockito.when(userService.getUserProcedure()).thenReturn(userDataList);

		Response resp = controller.getUserFromProc();

		Assert.assertNotNull(resp);
		Assert.assertEquals(201, resp.getStatus());
		Assert.assertNotNull(resp.getEntity());

	}
	@Test
	public void getKPIValues()  {
		KPI kpi =new KPI();
		kpi.setKpiKey("Net Sales");
		kpi.setCc_planned(1);
		kpi.setCc_forecast(1);
		kpi.setCc_ap_pcent(1);
		kpi.setCc_ap(1);
		kpi.setCc_actual(1);
		List<KPI> kpiList=new ArrayList<KPI>();
		kpiList.add(kpi);
		
		Mockito.when(userService.getKPIValues(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString())).thenReturn(kpiList);
		
		Response resp = controller.getKPIValues("YTD", "EMEA", "1", "2016");
		Assert.assertNotNull(resp);
		Assert.assertEquals(201, resp.getStatus());
		Assert.assertNotNull(resp.getEntity());
	}
}
