package org.javasavvy.rest.dao.test;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

import org.hibernate.internal.SessionImpl;
import org.javasavvy.rest.dao.UserDAOImpl;
import org.javasavvy.rest.entity.KPI;
import org.javasavvy.rest.entity.User;
import org.javasavvy.rest.modal.UserModal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
@RunWith(MockitoJUnitRunner.class)
public class UserDAOImplTest {
	
	@InjectMocks
	private UserDAOImpl dao;
	
	@Mock
	public EntityManager entityManager;
	
	 @Before
	    public void setUp() {
	        MockitoAnnotations.initMocks(this);
	    }
	
	@Test
	public void getUser(){
		TypedQuery mockedQuery = Mockito.mock(TypedQuery.class);
		
		List<User> userDataList=new ArrayList<>();
		  User user=new User();
		  user.setUser_name("ADMIN");
		  user.setStatus("ACTIVE");
		  user.setPwd("ADMIN");
		  userDataList.add(user);
		  
		Mockito.when(entityManager.createNamedQuery("fetchByName",User.class)).thenReturn(mockedQuery);
		
		Mockito.when(mockedQuery.setParameter(Mockito.anyString(),Mockito.anyString())).thenReturn(mockedQuery);
		Mockito.when(mockedQuery.setParameter(Mockito.anyString(),Mockito.anyString())).thenReturn(mockedQuery);
		Mockito.when(mockedQuery.getResultList()).thenReturn(userDataList);
		List<User> rsList = dao.getUser("ADMIN");
		Assert.assertNotNull(rsList);
		Assert.assertEquals(1,rsList.size());
		
	}
	@Test
	public void getStatusProcedure(){
		StoredProcedureQuery mockStoreProcQuery = Mockito.mock(StoredProcedureQuery.class);
		Mockito.when(entityManager.createNamedStoredProcedureQuery(Mockito.anyString())).thenReturn(mockStoreProcQuery);
		
		Mockito.when(mockStoreProcQuery.setParameter(Mockito.anyString(),Mockito.anyString())).thenReturn(mockStoreProcQuery);
		
		Mockito.when(mockStoreProcQuery.getOutputParameterValue(Mockito.anyString())).thenReturn("Valid");
		String status = dao.getStatusProcedure("ADMIN", "ADMIN");
		Assert.assertEquals("Valid",status);
		
	}
	
	@Ignore
	@Test
	public void getUserProcedure() throws SQLException{
		//SessionImpl mockSessionImpl = Mockito.mock(SessionImpl.class);
		Connection mockConnection = Mockito.mock(Connection.class);
		CallableStatement mockCStmt = Mockito.mock(CallableStatement.class);
		ResultSet mockRs = Mockito.mock(ResultSet.class);
		
		//Mockito.when(entityManager.unwrap(SessionImpl.class)).thenReturn(mockSessionImpl);
		//Mockito.when(mockSessionImpl.connection()).thenReturn(mockConnection);
		Mockito.when(mockConnection.prepareCall(Mockito.anyString())).thenReturn(mockCStmt);
		Mockito.when(mockCStmt.getResultSet()).thenReturn(mockRs);
		
		List<UserModal> status = dao.getUserProcedure();
		
		
	}
	
	@Test
	public void getKPIValues(){
		
		KPI kpi =new KPI();
		kpi.setKpiKey("Net Sales");
		kpi.setCc_planned(1);
		kpi.setCc_forecast(1);
		kpi.setCc_ap_pcent(1);
		kpi.setCc_ap(1);
		kpi.setCc_actual(1);
		List<KPI> kpiList=new ArrayList<KPI>();
		kpiList.add(kpi);
		
		Query mockQuery = Mockito.mock(Query.class);
		
		Mockito.when(entityManager.createNativeQuery(Mockito.anyString(), Mockito.any(Class.class))).thenReturn(mockQuery);
		Mockito.when(mockQuery.getResultList()).thenReturn(kpiList);
		List<KPI> kpiValues = dao.getKPIValues("YTD", "EMEA", "1", "2016");
		Assert.assertEquals("Net Sales",kpiValues.get(0).getKpiKey());
		
		
	}
	
	

}
